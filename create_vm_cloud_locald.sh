#!/bin/bash

nbVM=3
RAM=256
CPU=2
HDD="20G"

#adding color to output
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
NC='\033[0m'

#create folder to store config 
echo -e  "${BLUE}[INFO] => Create folder to store cloud-init stuff${NC}"
if [ -d "cloud_init.d" ]
then
	echo -e  "${BLUE} ----> Folder exist${NC}"
else

	mkdir "cloud_init.d"
	if [ "$?" = "0" ] ; then
		echo -e  "${GREEN} ----> Folder OK !${NC}"
		echo -e  "${BLUE}[INFO] => Set ACL on this folder${NC}"
		setfacl -m u:libvirt-qemu:r-x cloud_init.d
		if [ "$?" = "0" ] ; then
			echo -e  "${GREEN} ----> ACL OK !${NC}"
		else
			echo -e  "${ORANGE} ----> ACL Fucked up ! check if the filesystem support it ${NC}" 1>&2
		fi
	else
		echo -e  "${RED} ----> Folder Fucked up ! BRUH${NC}" 1>&2
		exit 1
	fi
fi
# creating folder to store img files
echo -e  "${BLUE}[INFO] => Create folder to store IMG files${NC}"
if [ -d "disks_images.d" ]
then
	echo -e  "${BLUE} ----> Folder exist${NC}"
else

	mkdir "disks_images.d"
	if [ "$?" = "0" ]; then
        	echo -e  "${GREEN} ----> Folder OK !${NC}"
			echo -e  "${BLUE}[INFO] => Set ACL on this folder${NC}"
			setfacl -m u:libvirt-qemu:r-x disks_images.d
			if [ "$?" = "0" ] ; then
				echo -e  "${GREEN} ----> ACL OK !${NC}"
			else
				echo -e  "${ORANGE} ----> ACL Fucked up ! check if the filesystem support it${NC}" 1>&2
			fi
	else
        	echo -e  "${RED} ----> Folder Fucked up ! BRUH${NC}" 1>&2
        	exit 1
	fi
fi

for idVM in $(seq 1 $nbVM)
do
	#Start of the loop
	echo -e  "${BLUE}[INFO] => Building VM $idVM ${NC}"
	#creating the image 
	echo -e  "${BLUE}[INFO] => Creating image..${PURPLE}"
	cd disks_images.d
	qemu-img create -b ../debian-10-cloud.qcow2 -f qcow2 -F qcow2 snapshot-debian-10-20G_n0de$idVM.qcow2 $HDD
	if [ "$?" = "0" ]; then
  		echo -e  "${GREEN} ----> Image done !${NC}"
	else
  		echo -e  "${RED} ----> Cannot create image !${NC}" 1>&2
  		exit 1
	fi
	#modify the cloud init vars
	echo -e  "${BLUE}[INFO] => Generating cloud-init files${NC}"
	#change hostname 
	cd ../cloud_init.d
	sed "s/<HOSTNAME>/Node$idVM/g" ../cloud_init.cfg > cloud_init_Node$idVM.cfg
	if [ "$?" = "0" ]; then
  		echo -e  "${GREEN} ----> Setting Hostname OK !${NC}"
	else
  		echo -e  "${RED} ----> Cannot set hostname !${NC}" 1>&2
  		exit 1
	fi
	#set ip address
	echo -e  "${BLUE}[INFO] => Setting Static IP${NC}" 
	sed "s/<ID>/1$idVM/g" ../network_config_static.cfg > network_config_static_Node$idVM.cfg
	if [ "$?" = "0" ]; then
  		echo -e  "${GREEN} ----> Setting IP OK !${NC}"
	else
  		echo -e  "${RED} ----> Cannot set IP !${NC}" 1>&2
  		exit 1
	fi
	cd ..
	# create cloud init image
	echo -e  "${BLUE}[INFO] => Creating cloud init image ${PURPLE}"
	cloud-localds -v --network-config=cloud_init.d/network_config_static_Node$idVM.cfg \
	disks_images.d/seed_test_Node$idVM.img cloud_init.d/cloud_init_Node$idVM.cfg
	if [ "$?" = "0" ]; then
                echo -e  "${GREEN} ----> Cloud init image DONE${NC}"
        else
                echo -e  "${RED} ----> Cloud init image failed ! Bruh !${NC}" 1>&2
                exit 1
        fi
	# Install the VM 
	echo -e  "${BLUE}[INFO] => Install the VM ${PURPLE}"
	virt-install --name Kube_Node$idVM --virt-type kvm --memory $RAM --vcpus $CPU \
	--boot hd,menu=on --disk path=disks_images.d/seed_test_Node$idVM.img,device=cdrom \
	--disk path=disks_images.d/snapshot-debian-10-20G_n0de$idVM.qcow2,device=disk \
	--graphics none --os-variant debian9 --network network:default \
	--console pty,target_type=serial --noautoconsole 
	if [ "$?" = "0" ]; then
                echo -e  "${GREEN} ----> VM created !${NC}"
        else
                echo -e  "${RED} ----> VM Fucked Up !${NC}" 1>&2
                exit 1
        fi


done
